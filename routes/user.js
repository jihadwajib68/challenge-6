const express = require('express');
const db = require('../models');
const router = express.Router();
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false}));

router.get('/', function (req, res) {
    res.render('register');
})

router.post('/', (req, res) => {
    db.user_game.create({
        user_name: req.body.username,
        user_pass: req.body.password,
        admin: false
    }).then(result => {
        console.log(result);
        db.user_biodata.create({
            full_name:req.body.fullname,
            gender: req.body.gender,
            email: req.body.email,
            phone: req.body.phone,
            userGameID: result.id
        }).then(biodata => {
            console.log(biodata);
        })
        res.redirect('/dashboard')
    })
})

router.post('/find/:id', (req, res) => {
    db.user_game.findOne({
        where: { id: req.params.id},
        include: [db.user_history, db.user_biodata]
    })
    .then(result => {
        res.render('edit', {result})
    })
})

router.post('/edit/:id', (req, res) => {
    db.user_game.update({
        user_name: req.body.username,
        user_pass: req.body.password,
        admin: false
    }, {
        where: { id: req.params.id}
    })
    .then(user => {
        db.user_biodata.update({
            full_name: req.body.fullname,
            gender: req.body.gender,
            email: req.body.email,
            phone: req.body.phone
        }, {
            where: { userGameID: req.params.id}
        })
        .then(result => {
            console.log(result);
            console.log('User sudah diupdate')
            res.redirect('/dashboard')
        })
    })
})

router.post('/delete/:id', (req, res) => {
    db.user_biodata.destroy({
        where: { id: req.params.id}
    })
    .then(() => {
        console.log('Biodata sudah dihapus')
        db.user_game.destroy({
            where: {id: req.params.id}
        })
        .then(() => {
            console.log('User sudah dihapus');
            res.render('delete');
        })
    })
})

module.exports = router