const express = require('express');
const morgan = require('morgan');
const score =  require('./routes/score');
const user = require('./routes/user');
const api = require('./api/api');
const db = require('./models');
const app = express();
const port = 5000;

let loginState = false;

app.set('view engine', 'ejs');
app.set('views', './views');

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/public', express.static(__dirname + '/public'));
app.use('/dashboard/score', score);
app.use('/dashboard/register', user);
app.use('/api/v1/users', api);

app.get('/login', (req, res) => {
    loginState = false;
    res.render('login');
})

app.post('/login', (req, res) => {
    const { username, password } = req.body;
    db.user_game.findOne({
        where: { user_name: username, user_pass: password, admin: true}
    }).then(result => {
        if(result) {
            console.log('Success Login');
            loginState= true;
            res.redirect('/dashboard')
            return;
        } else {
            console.log('failed Login');
            loginState = false;
            res.render('login', {
                message: 'Invalid username or password',
            });
        }
    })
});

let allUsers = [];

app.get('/dashboard', (req, res) => {
    db.user_game.findAll({
        where: { admin: false},
        include: [db.user_history, db.user_biodata]
    }).then(result => {
        allUsers = Array.from(result);
        res.render('dashboard', {
            allUsers
        })
    });
})

app.post('/dashboard', (req, res) => {
    db.user_game.findAll({
        include: [db.user_history, db.user_biodata]
    }).then(result => {
        let users = [];
        result.forEach(user => {
            if(req.body.fullname === user.user_biodatum.full_name) {
                users.push(user);
                console.log('add user' + user.user_name);
            }
        })
        allUsers = Array.from(users);
        res.render('dashboard', {
            allUsers
        })
    });
})

app.get('/test', (req, res) => {
    db.user_game.findAll({
        where: { admin: false},
        include: [db.user_history, db.user_biodata]
    }).then(result => {
        res.send(result);
        console.log(result);
    })
})

app.use(function(req, res) {
    res.status(404);
    res.send({
        status: 'Page not found!',
    })
});

app.use(function(err, req, res, next) {
    console.log('error');
    res.status(501).send({
        status: 'failed',
        error: err.message
    })
})

app.listen(port, () => {
    console.log('Anda menggunakan port 5000')
})